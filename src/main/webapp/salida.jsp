<%-- 
    Document   : salida
    Created on : 04-04-2020, 17:35:43
    Author     : Francisca
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Calculadora de interés simple</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
        <link rel="stylesheet" type="text/css" href="style.css"/>
    </head>
    <body>
        <div class="generalcontainer">
            
            <div class="subtitle">
                <h3 align="center">Taller de Aplicaciones Empresariales</h3>
                <h3 align="center">Alumna: Francisca Brinckfeldt Fernández</h3>
            </div>
            
            <div class="title">
                <h1 align="center"><b>Calculadora de Interés Simple</b></h1>
            </div>

            <%
                String amount = (String) request.getAttribute("amounts");
                String rate = (String) request.getAttribute("rates");
                String year = (String) request.getAttribute("years");
                String interesSimple = (String) request.getAttribute("interesSimple");
            %>
            
            
            <div class="salidacontainer">
                <div class="columns is-mobile">
                    <div class="column is-half is-offset-one-quarter">
                        <div class="content">
                            <p align="center">Para un capital de $<%=amount%>, una tasa del <%=rate%>% y <%=year%> años.</p>
                            <div class="box">
                                <h2 align="center">
                                    El interés simple es de: <br>
                                    $<%=interesSimple%>  
                                </h2>                              
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>  
    </body>
</html>
