/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.model;

/**
 *
 * @author Francisca
 */
public class CalcularInteresSimple {
    
    public float interesSimple(float amount, float rate, float year) {
        float interesSimple = amount*(rate/100)*year;
        return interesSimple;
    }
    
}
