<%-- 
    Document   : index
    Created on : 04-04-2020, 16:32:12
    Author     : Francisca
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Calculadora de interés simple</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
        <link rel="stylesheet" type="text/css" href="style.css"/>
    </head>
    <body> 
        <div class="generalcontainer">

            <div class="subtitle">
                <h3 align="center">Taller de Aplicaciones Empresariales</h3>
                <h3 align="center">Alumna: Francisca Brinckfeldt Fernández</h3>
            </div>
            
            <div class="title">
                <h1 align="center"><b>Calculadora de Interés Simple</b></h1>
            </div>
            

            <form name="form" action="salida" method="POST">
                <div class="container">
                    <div class="columns is-mobile">
                        <div class="column is-4 is-offset-4">
                            <div class="inputcontainer">
                                <div class="field">
                                    <label class="label">Capital:</label>
                                    <div class="control">
                                      <input class="input" type="number" name="amount" placeholder="Ingresa el monto del capital">
                                    </div>  
                                </div>
                             </div>

                            <div class="inputcontainer">
                                <label class="label">Tasa de interés anual:</label>
                                <div class="field has-addons">
                                    <div class="control" style="width: 100%;">
                                      <input class="input" type="number" name="rate" placeholder="Ingresa la tasa de interés anual">
                                    </div>
                                    <div class="control">
                                        <span class="button is-static">
                                            %
                                        </span>
                                    </div>
                                 </div>
                            </div>

                            <div class="inputcontainer">
                                <div class="field">
                                    <label class="label">Número de años:</label>
                                    <div class="control">
                                      <input class="input" type="number" name="year" placeholder="Ingresa el número de años">
                                    </div>
                                 </div>
                            </div>

                            <div class="field">
                                <div class="control">
                                    <div class="buttoncontainer">
                                        <input class="button is-link is-fullwidth" type="submit" value="Calcular">
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
            </form>
        </div>    
    </body>
</html>
